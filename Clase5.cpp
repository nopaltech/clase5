#include<iostream>
#include<cstring>

using namespace std;

int main(int argc, char* argv[]){
    // Estructuras de control repetitivas
    bool salir = false;
    int numero = 0;

    // Ejecuta codigo mientras que la evaluacion sea verdadera
    while(  !salir   ){
        numero++;
        cout << "Numero: " << numero << endl;

        if( numero >= 10 ){
            salir = true;
        }
    }

    salir = false;

    // Ejecuta codigo y luego evalua, si la evaluacion fue verdadera
    // continua ejecutando hasta encontrar un falso
    do{
        cout << "Ejecutado" << endl;
    }while( salir );

    // Se crea la variable y se le asigna valor dentro del for
    for(int i = 0; i < 10; i++){
        cout << "Numero: " << i << endl;
    }

    int inc;
    // Se le asigna valor a una variable existente
    for(inc = 10; inc > 0; inc--){
        cout << "Numero: " << inc << endl;
    }

    // Loop infinito
    /*while(true){

    }

    for(;;){

    }*/


    // Pedir dos numeros
    /*int num1, num2;
    cout << "Ingrese el numero 1: " << endl;
    cin >> num1;

    cout << "Ingrese el numero 2: " << endl;
    cin >> num2;

    cout << "El numero 1 fue: " << num1 << endl;
    cout << "El numero 2 fue: " << num2 << endl;*/


    // Pedir dos numeros a un usuario
    const int posiciones = 5;
    int numeros[posiciones]; //[int][int]

    //cout << "Tama�o de numeros: " << sizeof(numeros) / sizeof(int) << endl;

    int i;
    for(i = 0 ; i < sizeof(numeros) / sizeof(int); i++){
        cout << "Ingrese el numero " << i + 1 << ": " << endl;
        cin >> numeros[i];
    }

    cout << "Este es el valor de \"i\" del primer for: " << i << endl;

    for(int i = 0 ; i < sizeof(numeros) / sizeof(int); i++){
        cout << "El numero " << i + 1 << " fue: " << numeros[i] << endl;
    }


    char string_[45] = { "Orales! hhh " };

    char cadena[45] = {"Francisco Javier Alcala Olivares"};
    memset(cadena, 0, sizeof(cadena));
    strcpy(cadena, "Francisco Javier Alcala Olivares 0");
                    // seravilO .......
    // [][][][][][][][][][][]

    /*for(int i = 0; i < 45 ; i++){
        cout << "Cajita " << i << " contiene: " << cadena[i] << endl;
    }*/

    for(int i = sizeof(cadena) - 1; i >= 0; i-- ){
        if( cadena[i] != 0 ){
            cout << cadena[i];
        }
    }
    cout << endl;

    cout << "Fin" << endl;



    // 1.- Operacion este dentro un ciclo
    // hasta que el usuario decida salir

    // 2.- Declaren una variable de tipo array de char
    //       nombres <- 5 personas
    //       Persona 1 Juan Perez
    //       Persona 2 Luis Ramirez...

    // 3.- declarar una variable de tipo char
    // guardar un nombre y voltear el nombre
    //char nombre[45] = "Francisco Alcala";
    // codigo para voltear
    //cout << nombre << endl; // alaclA ...

    // 4.- Ingrese palabra
    // Es palindromo o no es palindormo
    //-> Sugus
    //-> Ana
    //-> Anita lava la tina
    //-> Dabale arroz a la zorra el abad


    return 0;
}
